FROM quay.io/keycloak/keycloak:17.0.1

# Make the realm configuration available for import
COPY imports/realm-iota.json /tmp/keycloak_import/realm-iota.json
# Import the realm and user
RUN /opt/keycloak/bin/kc.sh import --file /tmp/keycloak_import/realm-iota.json
# The Keycloak server is configured to listen on port 8080
EXPOSE 8080
