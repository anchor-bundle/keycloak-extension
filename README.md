# Keycloak Extension

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](./CODE_OF_CONDUCT.md)

This Keycloak Extension supports a 'Login with IOTA'

## Features

- This extension supports the login with DID and VC
- Resolves DID from tangle and verify the signature of the VC
- Role based access controll based on DID
- User is able to share only the data he wants to share with the client application

## Components

- Keycloak extension: extends Keycloak with the function to login with a DID
- "did-connect": Nodejs endpoint that is able to resolve and publish DID Documents from/to the Tangle (Devnet).

> CAUTION: this is only a quick workaround. Later the resolving/publishing and verifying of the signature is performed by an API, provided by Filancore

- Angular frontend: this litte project is an example website with the functionality to create DIDs/VCs and start an OpenId connect login request with it.

## Install

### Requirements

- Docker
to start the application:

```Shell
double click deploy&startKeycloak.bat
```

or manully:

```bash
./gradlew jar
docker-compose up
```

Keycloak and the Nodejs Endpoint are each running inside a docker container. They are both started with one docker-compose file.
The script 'deploy&startKeycloak.bat' is building the Keycloak extension (build a jar file) and launching Keycloak and the Nodejs Endpoint.

## Usage

The [angular application](https://gitlab.com/anchor-bundle/angular-webapp) is able to create a DID and VC for the user (if the user is not already having one). Then the angular application is starting an OpenId connect request to the Keycloak server. Then Keycloak can verify the signature of the VC and grand or decline access to the application.

## Architecture and Design

The Software Architecture and Requirements that were important for the development can be found [here](https://gitlab.com/anchor-bundle/angular-webapp/-/blob/main/documentation/README.md)

## Development

this project was developed in a docker dev container. In this container the development environment with all dependencys is already installed.
To use the dev container you need:

- Visual Studio Code
- Visual Studio Code Extension: Remote - Containers

Then it is possible to open this project in a dev container.

### Tests

The testframework used in this project is jasmine karma. To run the tests run the following command in the terminal:

```bash
./gradlew test
```

## Contributing

See [CONTRIBUTING](https://gitlab.com/anchor-bundle/angular-webapp/-/blob/main/CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
