@echo off
::build the jar with gradle
echo -----------------------
echo build jar for Keycloak extension
echo -----------------------
call gradlew jar
::start container
echo -----------------------
echo start container with Keycloak and deployed extension
echo -----------------------
docker-compose up