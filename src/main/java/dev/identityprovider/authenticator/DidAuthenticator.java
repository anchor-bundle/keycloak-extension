package dev.identityprovider.authenticator;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.Authenticator;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import dev.identityprovider.exceptions.ScopeNotAvailableException;
import dev.identityprovider.service.OIDCRequestHandlerService;
import dev.identityprovider.service.PresentationVerificationService;
import dev.identityprovider.service.UserManagementService;
import dev.identityprovider.service.VPHandlerService;
import twitter4j.JSONObject;

public class DidAuthenticator implements Authenticator {
    private final Logger log = Logger.getLogger(this.getClass().getName());

    HashMap<String, String> claimsMap = new HashMap<String, String>();
    List<String> requestedAttributes = new ArrayList<>();
    OIDCRequestHandlerService requestHandler = new OIDCRequestHandlerService();
    VPHandlerService vpHandlerService = new VPHandlerService();
    UserManagementService userManagementService = new UserManagementService();
    PresentationVerificationService presentationVerificationService;

    // this method is called if keycloak receives the OIDC call
    @Override
    public void authenticate(AuthenticationFlowContext context) {
        try {
            presentationVerificationService = new PresentationVerificationService(context.getSession());
            log.info("current flow path: " + context.getFlowPath());
            // get all requesteed Attributes from the OIDC all(over the scope)
            requestedAttributes = requestHandler.getRequestedUserAttributes(context);

            // getVP from Browser Cookie
            String decodedVP = requestHandler.getDecodedCookie(context);

            // get Timestamp from JSON VP
            JSONObject vpjson = new JSONObject(decodedVP);
            String timestamp = vpjson.getString("timestamp");

            // check if timestamp is newer than 5 minutes
            if (presentationVerificationService.isTimestampValid(timestamp)) {
                // check if vp is valid
                if (presentationVerificationService.verifyPresentation(vpjson)) {
                    // extrakt claims from json vp
                    vpHandlerService.getClaims(vpjson, claimsMap);
                    // send Formular to the User with the claims(user has to accept claims)
                    Response challenge = context.form()
                            .setAttribute("claims", claimsMap)
                            .setAttribute("requestedAttributes", requestedAttributes)
                            .createForm("did-authenticate.ftl");
                    context.challenge(challenge);
                } else {
                    // send error message to user
                    Response challenge = context.form()
                            .setError("Signature is not valid or did document could not be resolved")
                            .createForm("did-authenticateError.ftl");
                    context.failureChallenge(AuthenticationFlowError.ACCESS_DENIED, challenge);
                }
            } else {
                // send error because timestamp is not valid
                Response challenge = context.form()
                        .setError("Timestamp is not valid")
                        .createForm("did-authenticateError.ftl");
                context.failureChallenge(AuthenticationFlowError.ACCESS_DENIED, challenge);
            }
        } catch (UnsupportedEncodingException | ScopeNotAvailableException e) {
            Response challenge = context.form()
                    .setError("Error in getting or decoding VP")
                    .createForm("did-authenticateError.ftl");
            context.failureChallenge(AuthenticationFlowError.ACCESS_DENIED, challenge);
        }
    }

    // this method is executed, if user clicks on accept button
    @Override
    public void action(AuthenticationFlowContext context) {

        // get checkboxes the user has selected
        MultivaluedMap<String, String> decodedFormParameters = context.getHttpRequest().getDecodedFormParameters();
        HashMap<String, String> acceptedclaimsMap = new HashMap<String, String>();
        if (decodedFormParameters.containsKey("cancel")) {
            log.infof("user clicked on cancel");
            // user clicked on cancel
            context.cancelLogin();
            return;
        }

        // get claims the user accepted to share with the client
        requestHandler.getAcceptedClaims(decodedFormParameters, acceptedclaimsMap, claimsMap);

        // warnUserAttributesMissing(acceptedclaimsMap,context);

        // check if claims contain did(did is username)
        if (acceptedclaimsMap.containsKey("id")) {
            UserModel user = context.getSession().users().getUserByUsername(context.getRealm(),
                    acceptedclaimsMap.get("id"));
            // is user already in database?
            if (user == null) {
                log.infof("User [%s] not found in local database. Creating user ...", acceptedclaimsMap.get("id"));
                // create Subject and map attributes from claims:
                UserModel created = userManagementService.upsertUser(context, acceptedclaimsMap);
                context.setUser(created);
            } else {
                // user is available in Keycloak-> update claims
                UserModel created = userManagementService.upsertUser(context, acceptedclaimsMap);
                context.setUser(created);
                log.info("user is already available in Keycloak...update Claims");
            }
            context.success();

        } else {
            log.infof("did is missing in request");
            Response challenge = context.form()
                    .setAttribute("claims", claimsMap)
                    .setError("did is missing in request. Did is needed")
                    .createForm("did-authenticateError.ftl");
            context.failureChallenge(AuthenticationFlowError.CREDENTIAL_SETUP_REQUIRED, challenge);
        }

    }

    private void warnUserAttributesMissing(HashMap<String, String> acceptedclaimsMap,
            AuthenticationFlowContext context) {
        log.info("accepedclaims");
        acceptedclaimsMap.forEach((key, value) -> {
            log.info(key + " " + value);
        });
        log.info("requested attributes:");
        requestedAttributes.forEach(a -> {
            log.info(a);
        });
        List<String> missingAttributes = new ArrayList<String>();
        missingAttributes = requestedAttributes.stream().filter(attribute -> !acceptedclaimsMap.containsKey(attribute))
                .collect(Collectors.toList());
        missingAttributes.forEach(attribute -> {
            log.info("missing attribute: ");
            log.info(attribute);
        });

    }

    @Override
    public boolean requiresUser() {
        log.info("requiresUser: false");
        return false;
    }

    /**
     * Is user configured for this authenticator? --> always true
     */
    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        log.info("configuredFor()");
        // return getCredentialProvider(session).isConfiguredFor(realm, user,
        // getType(session));
        return true;
    }

    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {
    }

    @Override
    public void close() {
    }

}
