package dev.identityprovider.exceptions;
public class DidDocNotFoundException extends Exception {

    public DidDocNotFoundException() {
        super("DID document not found on tangle");
    }
    
}