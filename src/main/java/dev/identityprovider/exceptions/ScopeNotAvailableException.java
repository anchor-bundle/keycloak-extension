package dev.identityprovider.exceptions;

public class ScopeNotAvailableException extends Exception {

    public ScopeNotAvailableException() {
        super("scope is not available in the request");
    }
    
}
