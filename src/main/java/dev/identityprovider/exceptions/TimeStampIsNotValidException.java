package dev.identityprovider.exceptions;

public class TimeStampIsNotValidException extends Exception{
    public TimeStampIsNotValidException(){
        super("Timestamp is older then 30 minutes or not valid");
    }
}
