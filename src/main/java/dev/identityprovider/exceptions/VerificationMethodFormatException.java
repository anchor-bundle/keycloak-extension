package dev.identityprovider.exceptions;

public class VerificationMethodFormatException extends Exception {

    public VerificationMethodFormatException() {
        super("verificationMethod is missing or in the wrong Format");
    }

}
