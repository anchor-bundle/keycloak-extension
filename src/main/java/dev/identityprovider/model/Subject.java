package dev.identityprovider.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class Subject {
    String email;
    String username;
    String firstName;
    String lastName;
    String did;
}
