package dev.identityprovider.service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;

import dev.identityprovider.exceptions.ScopeNotAvailableException;

public class OIDCRequestHandlerService {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    /**
     * get cookie from browser and decode it
     * 
     * @param context
     * @return decoded Cookie as String
     * @throws UnsupportedEncodingException
     */
    public String getDecodedCookie(AuthenticationFlowContext context) throws UnsupportedEncodingException {
        Cookie cookie = context.getSession().getContext().getRequestHeaders().getCookies().get("VP");
        if (cookie != null) {
            String vp = cookie.getValue();
            // decode Cookie
            String decodedVP = "";
            // decode VP into UTF-8 String
            decodedVP = URLDecoder.decode(vp.replace("+", "%2B"), "UTF-8").replace("%2B", "+");
            log.infof(decodedVP);
            return decodedVP;
        } else {
            throw new Error("no vp cookie available");
        }

    }

    /**
     * get Attributes which are defined in the Scope settings for the OIDC Request
     * Scope
     * example: requested scope: town; in keycloak town has the user attribute town
     * and plz-> town and plz are the requested attributes
     * 
     * @param context
     * @return list with all requested Attributes
     * @throws ScopeNotAvailableException
     */
    public List<String> getRequestedUserAttributes(AuthenticationFlowContext context) throws ScopeNotAvailableException {
        // get Scopes from OIDC request
        String[] scopes = getScopes(context);
        List<String> allRequestedAttributed = new ArrayList<String>();
        // get all Scope Attributes from the Realm
        HashMap<String, List<String>> scopeAttributes = getScopeAttributesFromRealm(context);
        // check which scope is requested and what are its attributes:
        for (String scope : scopes) {
            List<String> attributes = scopeAttributes.get(scope);
            if (attributes != null) {
                allRequestedAttributed.addAll(attributes);
            }
        }
        return allRequestedAttributed;
    }

    /**
     * get every Attribute which is defined in the current Realm for different
     * scopes
     * 
     * @param context
     * @return Hashmap with scopes and its attributes
     *         example Hashmap:
     *         (profile| picture,lastName,firstName) -> scope profile has 3
     *         attributes
     *         (town| town,plz) -> scope town has 2 attributes
     */
    public HashMap<String, List<String>> getScopeAttributesFromRealm(AuthenticationFlowContext context) {
        HashMap<String, List<String>> scopesWithAttributes = new HashMap<String, List<String>>();
        context.getRealm().getClientScopesStream().forEach((scope) -> {
            List<String> userAttributes = new ArrayList<String>();
            scope.getProtocolMappersStream().forEach((mapper) -> {
                String userAttribute = mapper.getConfig().get("user.attribute");
                if (userAttribute != null) {
                    userAttributes.add(userAttribute);
                }
            });
            if (userAttributes.size() > 0) {
                scopesWithAttributes.put(scope.getName(), userAttributes);
            }
        });
        return scopesWithAttributes;
    }

    /**
     * get scopes from oidc request.
     * scopes are part of the URL(parameters)
     * 
     * @param context
     * @return String array with the requested scopes
     * @throws ScopeNotAvailableException
     */
    public String[] getScopes(AuthenticationFlowContext context) throws ScopeNotAvailableException {
        UriInfo uriInfo = context.getUriInfo();
        MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();
        String scopes = queryParameters.getFirst("scope");
        if (scopes == null) {
           throw new ScopeNotAvailableException();
        } else {
            return scopes.split(" ");
        }
    }

    /**
     * get claims the user accepted to share with the client
     * the accepted claims are filled in the hashmap acceptedClaimsMap
     * 
     * @param decodedFormParameters everything that is in the form the user send
     *                              (claims and accpet button)
     * @param acceptedclaimsMap     hashmap I store the accepted claims
     * @param claimsMap
     */
    public void getAcceptedClaims(MultivaluedMap<String, String> decodedFormParameters,
            HashMap<String, String> acceptedclaimsMap, HashMap<String, String> claimsMap) {

        for (String key : decodedFormParameters.keySet()) {
            // add accepted Items to different HashMap
            if (decodedFormParameters.get(key).get(0).equals("on") && claimsMap.containsKey(key)) {
                acceptedclaimsMap.put(key, claimsMap.get(key));
            }
        }
    }
}
