package dev.identityprovider.service;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.time.Instant;
import java.util.Base64;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.erdtman.jcs.JsonCanonicalizer;
import org.jboss.logging.Logger;
import org.keycloak.common.VerificationException;
import org.keycloak.connections.httpclient.HttpClientProvider;
import org.keycloak.models.KeycloakSession;
import org.web3j.crypto.Sign;
import org.web3j.utils.Numeric;

import dev.identityprovider.exceptions.DidDocNotFoundException;
import dev.identityprovider.exceptions.VerificationMethodFormatException;
import dev.identityprovider.util.ApplicationProfileLoader;
import dev.identityprovider.util.Base58;
import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 * Calls Filancore API to verify the VP
 * right now as workaround we call the nodejs endpoint to resolve the did
 * document
 */
public class PresentationVerificationService {

    private final Logger log = Logger.getLogger(this.getClass().getName());
    private final KeycloakSession session;

    private static final String FILANCORE_URL = ApplicationProfileLoader.getFilancoreUrl();

    public PresentationVerificationService(KeycloakSession session) {
        this.session = session;
    }

    /*
     * check if timestamp is valid (not older then 5 minutes)
     * parameter:
     * timestamp - String value of miliseconds since 1970
     * return
     * true or false, depending on the oldness of the timestamp
     */
    public boolean isTimestampValid(String timestamp) {
        Instant datenow = Instant.now();
        Instant dateTimestamp = Instant.ofEpochMilli(Long.valueOf(timestamp));
        long difference = datenow.toEpochMilli() - dateTimestamp.toEpochMilli();
        log.infof("differnece of time is " + difference);
        double minutesDifference = ((double) difference / 60000);
        if (minutesDifference < 5 && minutesDifference >= 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * verify the signature of the received vp and the vc in the vp
     * 
     * @param vpjson json object from the received vp(including proof part)
     * @return true or false, if the proof of vc and vp are valid
     * @throws VerificationException
     * @throws JSONException
     *
     */
    public boolean verifyPresentation(JSONObject vpjson) {
        try {
            // vp is changed during verification process. Later the origin object is needed
            JSONObject vpOrginal = new JSONObject(vpjson.toString());
            // check signature from VP

            if (verifySignatureFromJson(vpjson)) {
                log.info("vp signature is valid");
                // check signautre from VC
                JSONObject vc = vpjson.getJSONArray("verifiableCredential").getJSONObject(0);
                if (verifySignatureFromJson(vc)) {
                    log.info("vc signature is valid");
                    // check if proof did is the same as the credential subject did
                    if (isSignerVPTheCredentialSubject(vpOrginal)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    log.info("vp signautre is not valid");
                }
            } else {
                log.info("signature from VP is not valid");
            }
            return false;

        } catch (Exception e) {
            e.printStackTrace();
            log.info("error in verifying signature of vp");
           return false;
        }

    }

    /**
     * verify if the Signer from the VP also the credential subject
     * this is needed because anyone could sign a intercepted VC
     * 
     * @param vpjson
     * @throws VerificationException
     * @throws JSONException
     */
    private boolean isSignerVPTheCredentialSubject(JSONObject vpjson) throws JSONException, VerificationMethodFormatException {
        String didVPProof = getDIDFromProof(vpjson.getJSONObject("proof"));
        String didCredentialSubject = vpjson.getJSONArray("verifiableCredential").getJSONObject(0)
                .getJSONObject("credentialSubject").getString("id");
        if (didVPProof.equals(didCredentialSubject)) {
            log.info("did VP proof is the same one than the credentialSubject");
            return true;
        } else {
            log.info("did VP and credentialSubject are not the same!");
            return false;
        }
    }

    /**
     * verify the signature of a given Json Object
     * 
     * @param jsonObject vp or vc
     * @return if signature is valid
     */
    private boolean verifySignatureFromJson(JSONObject jsonObject) {
        try {
            JSONObject proof = jsonObject.getJSONObject("proof");
            // only able to check JsonWebSignature2020
            if (proof.has("type") && proof.getString("type").equals("JsonWebSignature2020") && proof.has("jws")) {
                // get signature

                String signatureHex = "0x" + getSignatureFromProof(proof);
                log.info("signature Hex: " + signatureHex);
                // remove proof section to get the plain text the user signed
                jsonObject.remove("proof");
                String jsonCanonicialized = "";

                JsonCanonicalizer jc = new JsonCanonicalizer(jsonObject.toString());
                jsonCanonicialized = jc.getEncodedString();

                String did = getDIDFromProof(proof);

                String publicKeyUser = getPublicKey(did);
                // check if signature is valid
                if (validateSignatureWithPublicKey(signatureHex, jsonCanonicialized, publicKeyUser)) {
                    log.info("signature of JSON valid");
                    return true;
                } else {
                    log.info("signature not valid");
                    return false;
                }
            } else {
                log.info("JSON has wrong proof format");
                return false;
            }
        } catch (Exception e) {
            log.info("error while verify signature");
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            log.info(sStackTrace);
            return false;
        }
    }

    /**
     * extract the did out of the vp
     * 
     * @param proof part of the received vp. Contains verificationMethod
     * @return did of the subject(user)
     * @throws VerificationException
     */
    public String getDIDFromProof(JSONObject proof) throws VerificationMethodFormatException {
        try {
            String verificationMethod = proof.getString("verificationMethod");
            String did = verificationMethod.substring(0, verificationMethod.indexOf("#key-1"));
            log.info("did: " + did);
            return did;
        } catch (JSONException | IndexOutOfBoundsException e) {
            throw new VerificationMethodFormatException();
        }

    }

    /**
     * get did document from tangle and extract public key
     * did document is received from nodejs endpoint
     * 
     * @param did did from iota tangle(did:iota:main:....)
     * @return public key from the given did
     * @throws Exception
     */
    private String getPublicKey(String did) throws Exception {
        HttpClient client = session.getProvider(HttpClientProvider.class).getHttpClient();
        try {
            HttpGet request = new HttpGet("http://did-connect:5555/users/resolveDIDDocument/" + did);
            HttpResponse response = client.execute(request);
            int statuscode = response.getStatusLine().getStatusCode();
            if (statuscode == 404){
                log.info("did document not found on tangle");
                throw new DidDocNotFoundException();
            }
            if (statuscode != 200) {
                throw new Exception("Error in nodejs endpoint");
            }
            log.info("statuscode: " + statuscode);
            InputStream contentBody = response.getEntity().getContent();
            String result = IOUtils.toString(contentBody, StandardCharsets.UTF_8);
            log.info("did doc");
            log.info(result);
            String publicKey = getPublicKeyFromDIDDoc(result);
            log.info("publicKey: " + publicKey);
            return publicKey;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while getting public Key");
        }
    }

    /**
     * extract public key from did document
     * 
     * @param didDoc did document from user
     * @return public key from user
     * @throws Exception
     */
    private String getPublicKeyFromDIDDoc(String didDoc) throws Exception {
        try {
            JSONObject diddoc = new JSONObject(didDoc);
            JSONObject publicKeyJWT = diddoc.getJSONArray("publicKey").getJSONObject(0).getJSONObject("publicKeyJwk");
            String publicKeyX = publicKeyJWT.getString("x");
            String publicKeyY = publicKeyJWT.getString("y");
            byte[] decodedX = Base58.decode(publicKeyX);
            byte[] decodedY = Base58.decode(publicKeyY);
            String publicKey = Hex.encodeHexString(decodedX) + Hex.encodeHexString(decodedY);
            if (publicKey.length() == 130 && publicKey.charAt(0) == 4) {
                return publicKey.substring(1, publicKey.length());
            } else {
                return publicKey;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while getting public Key from DID Document");
        }

    }

    /**
     * validates a singed message against the plain text with the public key
     * 
     * @param signedMessage:  this is the signed Message returned by a
     *                        wallet (signed String with private key)
     *                        singed Message with 0x
     * @param originalMessage this is the plaintext WITHOUT!! the ethereum
     *                        prefix(x19 Signed Mess...)
     * @param publicKey       public key from the person who signed the
     *                        signedMessage.
     *                        public key without 0x and without 04,03,...
     * @return if the signature is correct
     * @throws SignatureException
     */
    public boolean validateSignatureWithPublicKey(String signedMessage, String originalMessage,
            String publicKey)
            throws SignatureException {
        // message must be in hex format
        originalMessage = toHex(originalMessage);
        byte[] messageHashBytes = Numeric.hexStringToByteArray(originalMessage);
        String r = signedMessage.substring(0, 66);
        String s = signedMessage.substring(66, 130);
        String v = "0x" + signedMessage.substring(130, 132);
        byte[] msgBytes = new byte[messageHashBytes.length];
        System.arraycopy(messageHashBytes, 0, msgBytes, 0, messageHashBytes.length);

        String pubkey = Sign.signedPrefixedMessageToKey(msgBytes,
                new Sign.SignatureData(Numeric.hexStringToByteArray(v)[0],
                        Numeric.hexStringToByteArray(r),
                        Numeric.hexStringToByteArray(s)))
                .toString(16);
        log.info("expected publicKey: " + pubkey);
        if (pubkey.equals(publicKey)) {
            return true;
        } else {
            return false;
        }
    }

    // input String -> output hex value of this string
    public static String toHex(String arg) {
        return String.format("%x", new BigInteger(1, arg.getBytes(StandardCharsets.UTF_8)));
    }

    /*
     * extrakt the Signature out of the proof section from the received vp
     * parameters:
     * proof - part of the vp json and contains the proof
     * return:
     * signature in Hex format. (User signed vp without proof section =
     * signatureHex)
     */
    public String getSignatureFromProof(JSONObject proof) {
        String jws = proof.getString("jws");
        // cut Signature out of the jwt
        String signature = jws.substring(jws.indexOf("..") + 2, jws.length());
        // decode singnature base64
        byte[] signatureDecoded = Base64.getDecoder().decode(signature);
        // convert byte signature to hex
        String signatureHex = Hex.encodeHexString(signatureDecoded);
        return signatureHex;
    }

    // not needed methods

    public boolean verifyPresentationWithAPI(JSONObject verifiablePresentation) {
        log.infof("Verifying credential from holder=[%s] ...", verifiablePresentation.getString("holder"));
        try {
            HttpClient client = session.getProvider(HttpClientProvider.class).getHttpClient();
            // request
            HttpPost req = new HttpPost(FILANCORE_URL + "/identities");
            StringEntity requestEntity = new StringEntity(verifiablePresentation.toString(),
                    ContentType.APPLICATION_JSON);
            req.setEntity(requestEntity);// body

            // response
            HttpResponse res = client.execute(req);
            String body = EntityUtils.toString(res.getEntity());
            JsonReader reader = Json.createReader(new StringReader(body));
            JsonObject verificationResult = reader.readObject();
            reader.close();

            Boolean holderVerified = verificationResult
                    .getJsonObject("holder")
                    .getBoolean("verified");

            Boolean issuerVerified = verificationResult
                    .getJsonArray("credentials")
                    .getJsonObject(0)
                    .getJsonObject("issuer")
                    .getBoolean("verified");

            Boolean credentialsVerified = verificationResult
                    .getJsonArray("credentials")
                    .getJsonObject(0)
                    .getBoolean("verified");

            Boolean totalVerified = verificationResult
                    .getBoolean("verified");

            return holderVerified && issuerVerified && credentialsVerified && totalVerified;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * get AccessToken for Filancore API
     * Access token is needed to make further api calls
     * return: accesstoken
     */
    private String getAccessToken() {
        try {
            log.info("getting access token");
            HttpClient client = session.getProvider(HttpClientProvider.class).getHttpClient();
            // request
            HttpPost req = new HttpPost(FILANCORE_URL + "/auth/login");
            log.info(FILANCORE_URL);
            JSONObject requestBody = new JSONObject();
            requestBody.put("passwordIdentifier", "testing_4@filancore.net");
            requestBody.put("password", "");
            requestBody.put("method", "password");
            StringEntity requestEntity = new StringEntity(requestBody.toString(), ContentType.APPLICATION_JSON);
            req.setEntity(requestEntity);// body

            // response
            HttpResponse res = client.execute(req);
            String body = EntityUtils.toString(res.getEntity());
            JsonReader reader = Json.createReader(new StringReader(body));
            JsonObject responseJson = reader.readObject();
            reader.close();
            String accessToken = responseJson.getString("accessToken");
            log.info("access Token:");
            log.infof(accessToken);
            return accessToken;
        } catch (Exception e) {
            log.info("error while getting access token:");
            e.printStackTrace();
        }
        return null;
    }
}
