package dev.identityprovider.service;

import java.util.HashMap;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.models.UserModel;

public class UserManagementService {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    /**
     * insert new User into Keycloak (with Claims) or update existing User
     * 
     * @param context           AuthenticationFlowContext from Keycloak
     * @param acceptedclaimsMap all claims the User accepted to share with the
     *                          client Application
     * @return
     */
    public UserModel upsertUser(AuthenticationFlowContext context, HashMap<String, String> acceptedclaimsMap) {
        UserModel user = context.getSession().users().getUserByUsername(context.getRealm(),
                acceptedclaimsMap.get("id"));
        if (user == null) {
            log.info("user not found");
            user = context.getSession().users().addUser(context.getRealm(), acceptedclaimsMap.get("id"));
        } else {
            log.info("user found in database");
            // delte every user propertiy, in clase user rejected some claims
            for (String key : user.getAttributes().keySet()) {
                user.removeAttribute(key);
            }
            user.setFirstName(null);
            user.setLastName(null);
            user.setEmail(null);
        }

        // fill user with claims
        for (String key : acceptedclaimsMap.keySet()) {
            switch (key) {
                case "email":
                    user.setEmail(acceptedclaimsMap.get("email"));
                    log.info("setze email ");
                    break;
                case "firstName":
                    user.setFirstName(acceptedclaimsMap.get(("firstName")));
                    log.info("setze firstName");
                    break;
                case "lastName":
                    user.setLastName(acceptedclaimsMap.get("lastName"));
                    log.info("setze lastName");
                    break;
                case "username":
                    // do nothing, keycloak username would be overwritten
                    break;
                case "id":
                    // ignore id because it is already the username
                    break;
                default:
                    log.info("setze attribut: " + key);
                    user.setSingleAttribute(key, acceptedclaimsMap.get(key));
                    break;
            }
        }
        user.setEnabled(true);
        return user;
    }
}
