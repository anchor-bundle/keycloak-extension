package dev.identityprovider.service;

import java.util.HashMap;

import twitter4j.JSONArray;
import twitter4j.JSONObject;

public class VPHandlerService {
    /*
     * extract claims from vp (json format)
     * claims are saved in global Hashmap: 'claimsMap'
     * parameters:
     * vpjson - verifiable presentation in json format(received from user)
     * return
     * nothing - claims Hashmap is filled in this method
     */
    public void getClaims(JSONObject vpjson, HashMap<String, String> claimsMap) {
        JSONArray vc = vpjson.getJSONArray("verifiableCredential");
        for (int i = 0; i < vc.length(); i++) {
            JSONObject object = vc.getJSONObject(i);
            JSONObject claims = object.getJSONObject("credentialSubject");
            // put claims into global claims Hashmap
            for (String claimKey : claims.keySet()) {
                claimsMap.put(claimKey, claims.get(claimKey).toString());
            }
        }

    }

}
