package dev.identityprovider.util;

import org.jboss.logging.Logger;

import java.io.IOException;
import java.util.Properties;

/**
 * Small implementation to load config properties from file (similar to Spring
 * Profiles).
 *
 * @see <a href="https://www.baeldung.com/spring-profiles">Spring Profiles</a>
 */
public class ApplicationProfileLoader {

    private final static Logger log = Logger.getLogger(ApplicationProfileLoader.class);

    private static Properties loadProperties(String profile) {
        Properties props = new Properties();
        try {
            if (profile == null) {
                props.load(ApplicationProfileLoader.class.getClassLoader()
                        .getResourceAsStream("iota-extension-application.properties"));
            } else {
                props.load(ApplicationProfileLoader.class.getClassLoader()
                        .getResourceAsStream("application-" + profile + ".properties"));
            }
            log.infof("Properties loaded [profile=%s].", profile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }

    public static String getFilancoreUrl() {
        String profile = System.getProperty("profile");
        Properties props = loadProperties(profile);
        String filancoreUrl = props.getProperty("filancore.url");
        if (filancoreUrl == null) {
            log.warn("No configuration found for \"filancore.url\".");
        }
        return filancoreUrl;
    }

}
