package dev.identityprovider.service;

import java.io.IOException;
import java.util.HashMap;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;
import org.junit.jupiter.api.Test;
import org.keycloak.authentication.AuthenticationFlowContext;

import dev.identityprovider.exceptions.ScopeNotAvailableException;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OIDCRequestHandlerServiceTest {
    AuthenticationFlowContext context = mock(AuthenticationFlowContext.class, RETURNS_DEEP_STUBS);
    OIDCRequestHandlerService oidcRequestHandlerService = new OIDCRequestHandlerService();
    private final Logger log = Logger.getLogger(this.getClass().getName());

    @Test
    void getDecodedCookieTest() throws IOException {
        // given
        Cookie cookie = new Cookie("VP",
                "%7B%0D%0A%20%20%20%22%40context%22%3A%5B%0D%0A%20%20%20%20%20%20%22https%3A%2F%2Fwww.w3.org%2F2018%2Fcredentials%2Fv1%22%0D%0A%20%20%20%5D%2C%0D%0A%20%20%20%22type%22%3A%5B%0D%0A%20%20%20%20%20%20%22VerifiablePresentation%22%0D%0A%20%20%20%5D%2C%0D%0A%20%20%20%22timestamp%22%3A1659862223315%2C%0D%0A%20%20%20%22verifiableCredential%22%3A%5B%0D%0A%20%20%20%20%20%20%7B%0D%0A%20%20%20%20%20%20%20%20%20%22%40context%22%3A%5B%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22https%3A%2F%2Fwww.w3.org%2F2018%2Fcredentials%2Fv1%22%0D%0A%20%20%20%20%20%20%20%20%20%5D%2C%0D%0A%20%20%20%20%20%20%20%20%20%22id%22%3A%22http%3A%2F%2Fexample.com%2Fcredentials%2F2120%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%22type%22%3A%5B%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22VerifiableCredential%22%0D%0A%20%20%20%20%20%20%20%20%20%5D%2C%0D%0A%20%20%20%20%20%20%20%20%20%22issuer%22%3A%22did%3Aiota%3Amain%3ABdr2vrXSFBuJTiR3CdFuxyRzN8MZg2eU1urSV8PzDsBx%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%22issuanceDate%22%3A1659681677528%2C%0D%0A%20%20%20%20%20%20%20%20%20%22proof%22%3A%7B%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22type%22%3A%22JsonWebSignature2020%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22created%22%3A1659681680555%2C%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22proofPurpose%22%3A%22assertionMethod%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22verificationMethod%22%3A%22did%3Aiota%3Amain%3ABdr2vrXSFBuJTiR3CdFuxyRzN8MZg2eU1urSV8PzDsBx%23key-1%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22jws%22%3A%22eyJhbGciOiJFUzI1NksiLCJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdfQ%3D%3D..ugkA4DdOIXMWfz1zI1DxO1u4VfQng1dPEiHBop%2BeZKtVU8fCdfLD3gJwkCfHCCs8CqbaPQBbYjCwXpeuCGKVVBs%3D%22%0D%0A%20%20%20%20%20%20%20%20%20%7D%2C%0D%0A%20%20%20%20%20%20%20%20%20%22credentialSubject%22%3A%7B%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22id%22%3A%22did%3Aiota%3Amain%3ABdr2vrXSFBuJTiR3CdFuxyRzN8MZg2eU1urSV8PzDsBx%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22first%20name%22%3A%22Tim%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22last%20name%22%3A%22Altmann%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22nickname%22%3A%22t.altmann%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22town%22%3A%22Konstanz%22%2C%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%22email%22%3A%22super.ti%40gmx.de%22%0D%0A%20%20%20%20%20%20%20%20%20%7D%0D%0A%20%20%20%20%20%20%7D%0D%0A%20%20%20%5D%2C%0D%0A%20%20%20%22proof%22%3A%7B%0D%0A%20%20%20%20%20%20%22type%22%3A%22JsonWebSignature2020%22%2C%0D%0A%20%20%20%20%20%20%22created%22%3A1659862245846%2C%0D%0A%20%20%20%20%20%20%22proofPurpose%22%3A%22assertionMethod%22%2C%0D%0A%20%20%20%20%20%20%22verificationMethod%22%3A%22did%3Aiota%3Amain%3ABdr2vrXSFBuJTiR3CdFuxyRzN8MZg2eU1urSV8PzDsBx%23key-1%22%2C%0D%0A%20%20%20%20%20%20%22jws%22%3A%22eyJhbGciOiJFUzI1NksiLCJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdfQ%3D%3D..C4DV83SkGHjubV03YCwj9MCFpa7Dq3QWD9sIJ4iYjxRiBf2WGKRmvhaA5a4pU3YoiWel0A4lc6iMro8l%2FruQixw%3D%22%0D%0A%20%20%20%7D%0D%0A%7D");
        // when
        when(context.getSession().getContext().getRequestHeaders().getCookies().get("VP")).thenReturn(cookie);

        // then
        assertTrue(oidcRequestHandlerService.getDecodedCookie(context).contains("verifiableCredential"));
    }

    @Test
    void getDecodedCookieTest_Null() {
        // given
        // when
        when(context.getSession().getContext().getRequestHeaders().getCookies().get("VP")).thenReturn(null);

        // then
        assertThrows(Error.class, () -> oidcRequestHandlerService.getDecodedCookie(context));
    }

    @Test
    void getScopeTest() throws ScopeNotAvailableException {
        // given
        UriInfo uriInfo = mock(UriInfo.class);
        MultivaluedMap<String, String> queryParameters = new MultivaluedHashMap<>();
        queryParameters.add("scope", "openid profile email offline_access town");
        // when
        when(context.getUriInfo()).thenReturn(uriInfo);
        when(context.getUriInfo().getQueryParameters()).thenReturn(queryParameters);

        // then
        String[] scopes = oidcRequestHandlerService.getScopes(context);
        boolean emailAvailable = false;
        boolean townAvailable = false;
        for (String scope : scopes) {
            if (scope.equals("email")) {
                emailAvailable = true;
            }
            if (scope.equals("town")) {
                townAvailable = true;
            }
        }
        assertTrue(emailAvailable);
        assertTrue(townAvailable);

    }

    @Test
    void getScopeTestNoScope() throws ScopeNotAvailableException {
        // given
        UriInfo uriInfo = mock(UriInfo.class);
        MultivaluedMap<String, String> queryParameters = new MultivaluedHashMap<>();
        // when
        when(context.getUriInfo()).thenReturn(uriInfo);
        when(context.getUriInfo().getQueryParameters()).thenReturn(queryParameters);

        // then
        assertThrows(ScopeNotAvailableException.class,() -> oidcRequestHandlerService.getScopes(context));

    }

    @Test
    void getAcceptedClaimsTest(){
        //given
        MultivaluedMap<String, String> decodedFormParameters = new MultivaluedHashMap<>();
        //add 2 parameters
        decodedFormParameters.add("firstName", "on");
        decodedFormParameters.add("town", "on");
        HashMap<String, String> acceptedclaimsMap = new HashMap<>();
        HashMap<String, String> claimsMap = new HashMap<>();
        // user accepted the claims
        claimsMap.put("firstName", "max");
        claimsMap.put("town", "berlin");
        //when

        //then
        oidcRequestHandlerService.getAcceptedClaims(decodedFormParameters, acceptedclaimsMap, claimsMap);
        assertTrue(acceptedclaimsMap.get("firstName").equals("max"));
        assertTrue(acceptedclaimsMap.get("town").equals("berlin"));
        assertTrue(acceptedclaimsMap.size()== 2);
    } 
    
    @Test
    void getAcceptedClaims_NothingAcceptedTest(){
        //given
        MultivaluedMap<String, String> decodedFormParameters = new MultivaluedHashMap<>();
        //add 2 parameters
        HashMap<String, String> acceptedclaimsMap = new HashMap<>();
        HashMap<String, String> claimsMap = new HashMap<>();
        // user accepted the claims
        claimsMap.put("firstName", "max");
        claimsMap.put("town", "berlin");
        //when

        //then
        oidcRequestHandlerService.getAcceptedClaims(decodedFormParameters, acceptedclaimsMap, claimsMap);
        assertTrue(acceptedclaimsMap.get("firstName") == null);
        assertTrue(acceptedclaimsMap.size()== 0);
    } 

}
