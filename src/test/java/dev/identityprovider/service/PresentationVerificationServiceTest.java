package dev.identityprovider.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import dev.identityprovider.KeycloakTestBase;
import dev.identityprovider.exceptions.VerificationMethodFormatException;
import twitter4j.JSONException;
import twitter4j.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.time.Instant;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.keycloak.common.VerificationException;
import org.keycloak.connections.httpclient.HttpClientProvider;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PresentationVerificationServiceTest extends KeycloakTestBase {

        PresentationVerificationService presentationVerificationService = new PresentationVerificationService(session);

        static WireMockServer wireMockServer;

        @BeforeAll
        static void setUp() {

        }

        @Test
        void isTimestampValidTest() {
                String timestampMili = String.valueOf(Instant.now().toEpochMilli());
                // this timestamp should be accepted
                assertTrue(presentationVerificationService.isTimestampValid(timestampMili));
                String timestampMilisInFuture = String.valueOf(Instant.now().toEpochMilli() + 5000);
                assertFalse(presentationVerificationService.isTimestampValid(timestampMilisInFuture)); // should be
                                                                                                       // false,
                                                                                                       // because
                                                                                                       // timestamp is
                                                                                                       // from the
                // future
                String timestampMilisToOld = String.valueOf(Instant.now().toEpochMilli() - (6 * 60000));// timestamp is
                                                                                                        // 6
                                                                                                        // minutes old
                assertFalse(presentationVerificationService.isTimestampValid(timestampMilisToOld)); // should be false,
                                                                                                    // because
                                                                                                    // timestamp is from
                                                                                                    // the
                // future

                assertThrows(Exception.class, () -> presentationVerificationService.isTimestampValid("timestamp"));

        }

        @Test
        void getSignatureFromProofTest() throws IOException {
                // given

                JSONObject proof = new JSONObject();
                String signatureString = "eyJhbGciOiJFUzI1NksiLCJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdfQ==..DC3nkci9cjoaMeGS00QC7zhw3vOJ77Fi2x0wjP7hPT8kUtqA2ikndWzEaaH2PSoEUgXcMrVGcoy6KE906l9aEhw=";
                proof.put("jws", signatureString);
                String expectedSignature = "0c2de791c8bd723a1a31e192d34402ef3870def389efb162db1d308cfee13d3f2452da80da2927756cc469a1f63d2a045205dc32b546728cba284f74ea5f5a121c";
                JSONObject proof2 = new JSONObject();
                JSONObject proof3 = new JSONObject();
                proof3.put("jws", "");
                // when

                // then
                String signature = presentationVerificationService.getSignatureFromProof(proof);
                assertTrue(signature.equals(expectedSignature));
                assertThrows(JSONException.class, () -> presentationVerificationService.getSignatureFromProof(proof2));
                assertThrows(NullPointerException.class,
                                () -> presentationVerificationService.getSignatureFromProof(null));
                assertThrows(IndexOutOfBoundsException.class,
                                () -> presentationVerificationService.getSignatureFromProof(proof3));

        }

        @Test
        void validateSignatureWithPublicKeyTest() throws SignatureException {
                // given
                String signature = "0xd6b525106ef38260337396a07944dc6340bb99d3ebebe1962445fb343df6f5e131e92c013d3f406bef616dbb02abfaefb8f773dc0ef859f2a9d9b13d3e937ab11b";
                String message = "test";
                String publicKey = "6d5012a3515a5c325c32faf026e5f3943b2fb07ebb9d5e93170e60daca927961a73ace7c88db3bda4310c9db2d7b2cd2360374bdd4fd0b7f9d65de87ff98245c";
                // when

                // then
                assertTrue(presentationVerificationService.validateSignatureWithPublicKey(signature,
                                message, publicKey));
                assertFalse(presentationVerificationService.validateSignatureWithPublicKey(signature,
                                "example", publicKey));
                assertThrows(NullPointerException.class,
                                () -> presentationVerificationService.validateSignatureWithPublicKey(null, "", null));
                assertFalse(presentationVerificationService.validateSignatureWithPublicKey(signature,
                                "", null));

        }

        @Test
        void getDIDFromVPTest() throws VerificationMethodFormatException {
                // given
                JSONObject proof = new JSONObject();
                JSONObject proof2 = new JSONObject();
                JSONObject proof3 = new JSONObject();
                proof.put("verificationMethod", "did:iota:main:Bdr2vrXSFBuJTiR3CdFuxyRzN8MZg2eU1urSV8PzDsBx#key-1");
                proof3.put("verificationMethod", "did:iota:main:Bdr2vrXSFBuJTiR3CdFuxyRzN8MZg2eU1urSV8PzDsBx");
                String did = presentationVerificationService.getDIDFromProof(proof);
                // when

                // then
                assertTrue(did.equals("did:iota:main:Bdr2vrXSFBuJTiR3CdFuxyRzN8MZg2eU1urSV8PzDsBx"));
                assertThrows(VerificationMethodFormatException.class,
                                () -> presentationVerificationService.getDIDFromProof(proof2));
                assertThrows(VerificationMethodFormatException.class,
                                () -> presentationVerificationService.getDIDFromProof(proof3));
        }

        @Test
        void verifyPresentationTest() throws IOException {
                // given
                InputStream is = PresentationVerificationServiceTest.class.getClassLoader()
                                .getResourceAsStream("valid-presentation.json");
                String validVp = IOUtils.toString(is, StandardCharsets.UTF_8);
                validVp = validVp.replace("\t", "").replace("\n", "").replace("\r", "");
                JSONObject vpjson = new JSONObject(validVp);

                // invalid vp:
                InputStream is1 = PresentationVerificationServiceTest.class.getClassLoader()
                                .getResourceAsStream("notvalid-presentation.json");
                String notvalidVp = IOUtils.toString(is1, StandardCharsets.UTF_8);
                notvalidVp = notvalidVp.replace("\t", "").replace("\n", "").replace("\r", "");
                JSONObject vpjsoninvalid = new JSONObject(notvalidVp);

                // empty vp:
                JSONObject emptyvp = new JSONObject();
                // when
                CloseableHttpClient client = mock(CloseableHttpClient.class);
                CloseableHttpResponse response = mock(CloseableHttpResponse.class);
                StatusLine statusLine = mock(StatusLine.class);
                HttpEntity httpEntity = mock(HttpEntity.class);
                when(session.getProvider(HttpClientProvider.class).getHttpClient()).thenReturn(client);
                when(client.execute(any(HttpUriRequest.class))).thenReturn(response);
                when(response.getStatusLine()).thenReturn(statusLine);
                when(statusLine.getStatusCode()).thenReturn(200);
                when(response.getEntity()).thenReturn(httpEntity);
                /*
                 * when(httpEntity.getContent()).thenReturn(PresentationVerificationServiceTest.
                 * class.getClassLoader()
                 * .getResourceAsStream("valid-DIDDoc.json"));
                 */
                when(httpEntity.getContent()).thenAnswer(new Answer() {

                        @Override
                        public Object answer(InvocationOnMock invocation) throws Throwable {
                                // TODO Auto-generated method stub
                                return PresentationVerificationServiceTest.class.getClassLoader()
                                                .getResourceAsStream("valid-DIDDoc.json");
                        }

                });

                // then
                assertTrue(presentationVerificationService.verifyPresentation(vpjson));
                assertFalse(presentationVerificationService.verifyPresentation(vpjsoninvalid));
                assertFalse(presentationVerificationService.verifyPresentation(emptyvp));

        }

    @Test
    void isSignerVPTheCredentialSubjectTest() throws IOException {
        // given
        InputStream is = PresentationVerificationServiceTest.class.getClassLoader()
        .getResourceAsStream("valid-presentation.json");
        String validVp = IOUtils.toString(is, StandardCharsets.UTF_8);
        validVp = validVp.replace("\t", "").replace("\n", "").replace("\r", "");
        JSONObject vpjson = new JSONObject(validVp);
        JSONObject vpjsonWithoutID = new JSONObject(validVp);
        //change did of vc
        vpjson.getJSONArray("verifiableCredential").getJSONObject(0)
                .getJSONObject("credentialSubject").remove("id");
        vpjson.getJSONArray("verifiableCredential").getJSONObject(0)
                .getJSONObject("credentialSubject").put("id", "did:iota:example12344");
        
        //remove id field
        vpjsonWithoutID.getJSONArray("verifiableCredential").getJSONObject(0)
        .getJSONObject("credentialSubject").remove("id");        
        //when


        //then
        assertFalse(presentationVerificationService.verifyPresentation(vpjson));
        assertFalse(presentationVerificationService.verifyPresentation(vpjsonWithoutID));
    }

}
