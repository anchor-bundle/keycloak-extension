package dev.identityprovider.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import dev.identityprovider.KeycloakTestBase;

public class UserManagementServiceTest extends KeycloakTestBase {
    UserManagementService userManagementService = new UserManagementService();

    @Test
    void upsertUserTest() {
        // given
        UserModel user = mock(UserModel.class);
        HashMap<String, String> acceptedclaimsMap = new HashMap<>();
        acceptedclaimsMap.put("email", "max.mustermann@hotmail.com");
        acceptedclaimsMap.put("firstName", "Max");
        acceptedclaimsMap.put("id", "did:1234");

        // when
        when(context.getSession().users().getUserByUsername(any(RealmModel.class), anyString())).thenReturn(user);
        UserModel generatedUser = userManagementService.upsertUser(context, acceptedclaimsMap);
        // then
        verify(user).setEmail("max.mustermann@hotmail.com");
        verify(user).setFirstName("Max");
        verify(user).setEmail(null);
        verify(user).setFirstName(null);
        assertTrue(user.equals(generatedUser));
    }

    @Test
    void upsertUserTest_CreateNewUser() {
        // given
        HashMap<String, String> acceptedclaimsMap = new HashMap<>();
        acceptedclaimsMap.put("firstName", "Max");
        acceptedclaimsMap.put("id", "did:1234");

        // when
        when(context.getSession().users().getUserByUsername(any(RealmModel.class), anyString())).thenReturn(null);
        UserModel generatedUser = userManagementService.upsertUser(context, acceptedclaimsMap);
        // then
        verify(context.getSession().users()).addUser(any(RealmModel.class), anyString());
        assertTrue(generatedUser != null);
    }

}
