package dev.identityprovider.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import twitter4j.JSONException;
import twitter4j.JSONObject;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VPHandlerServiceTest {
    VPHandlerService vpHandlerService = new VPHandlerService();

    @Test
    void getClaimsTest() throws IOException {
        // given
        HashMap<String, String> claimsMap = new HashMap<>();
        InputStream is = VPHandlerServiceTest.class.getClassLoader().getResourceAsStream("valid-presentation.json");
        String result = IOUtils.toString(is, StandardCharsets.UTF_8);
        JSONObject vpjson = new JSONObject(result);
        // when
        vpHandlerService.getClaims(vpjson, claimsMap);

        // then
        assertTrue(claimsMap.containsKey("town"));
        assertTrue(claimsMap.containsKey("email"));
    }

    @Test
    void getClaimsTestInvalid() throws IOException {
        // given
        HashMap<String, String> claimsMap = new HashMap<>();
        InputStream is = VPHandlerServiceTest.class.getClassLoader()
                .getResourceAsStream("notvalid-presentation.json");
        String result = IOUtils.toString(is, StandardCharsets.UTF_8);
        JSONObject vpjson = new JSONObject(result);
        // when

        // then
        assertThrows(JSONException.class, () -> vpHandlerService.getClaims(vpjson, claimsMap));
    }

}
