<#import "template.ftl" as layout>
<#import "components/button/primary.ftl" as buttonPrimary>

<@layout.registrationLayout; section>
    <#if section = "title">
        ${msg("loginTitle",realm.name)}
    <#elseif section = "header">
        <#-- ${msg("loginTitleHtml",realm.name)} -->
    <#elseif section = "form">
        <!-- Form -->
        <form id="kc-totp-login-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
            <div class="${properties.kcFormGroupClass!}">
            </div>
			<h3>this data will be send to the client</h3>
            <div style="display: flex; justify-content: center; flex-direction:column">
                <#list claims?keys as prop>
                
                <div style="display: flex; justify-content: left; padding: 5px;" >
                <#if prop =="id">
                <input style=" filter: invert(100%) hue-rotate(147deg) brightness(1.7);" type="checkbox" id="${prop}" name="${prop}" readonly="readonly" checked>
                <label style= "margin-left:5px; margin-right:5px; color: darkgrey" for="${prop}">${prop} (required)</label>
                <#else>
                <input type="checkbox" id="${prop}" name="${prop}" checked>
                    <label style= "margin-left:5px; margin-right:5px;" for="${prop}">${prop} ${requestedAttributes?seq_contains(prop)?string("(requested)", "(optional)")}</label>
                </#if> 
                    <details style="margin-left:5px;">
                    <summary></summary>
                    <p>${claims[prop]}</p>
                    </details>
                </div>  
				</#list>
            </div>
            <br>
            <div>
                <label style= "margin-left:5px; margin-right:5px;">Application requested this attributes: </label>
                <details style="margin-left:5px;">
                <summary></summary>
                 <#list requestedAttributes as attribute>
                <p>${attribute} ${claims?keys?seq_contains(attribute)?string("", "(missing in vp)")}</p>
                </#list>
                </details>
            </div>
            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                    </div>
                </div>

                <@buttonPrimary.kw name="login" type="submit" style="background-color: #0fc1b7; margin-bottom:5px;">
                    Accept
                </@buttonPrimary.kw>  
                <@buttonPrimary.kw name="cancel" type="submit" style="background-color: #fc0303">
                    Cancel
                </@buttonPrimary.kw>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
