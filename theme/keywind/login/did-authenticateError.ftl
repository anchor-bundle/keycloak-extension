<#import "template.ftl" as layout>
<#import "components/button/primary.ftl" as buttonPrimary>

<@layout.registrationLayout; section>
    <#if section = "title">
        ${msg("loginTitle",realm.name)}
    <#elseif section = "header">
        <#-- ${msg("loginTitleHtml",realm.name)} -->
    <#elseif section = "form">
        <!-- Form -->
    </#if>
</@layout.registrationLayout>
